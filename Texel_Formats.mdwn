Like many GPUs, the Mali GPU supports a wide range of texel formats:

                 alpha   flags    components
     id bpp byt  ia  ha  rb  ro   r   g   b   a   d   s   l   i   note

     00   1   1                                           1    
     01   1   1   +   +                       1                
     02   1   1                                               1
     03   2   1       +       +               1           1    
     04   4   1                                           4    
     05   4   1   +   +                       4                
     06   4   1                                               4
     07   4   1       +   +   +   1   1   1   1                
     08   8   1       +       +               4           4    
     09   8   1                                           8    
     0A   8   1   +   +                       8                
     0B   8   1                                               8
     0C   8   1           +       3   3   2                    
     0D   8   1       +   +   +   2   2   2   2                
     0E  16   2           +       5   6   5                    
     0F  16   2       +   +   +   5   5   5   1                
     10  16   2       +   +   +   4   4   4   4                
     11  16   1       +       +               8           8    
     12  16   2                                          16    
     13  16   2   +   +                      16                
     14  16   2                                              16
     15 N/A   1                   8   8   8                    
     16  32   1       +   +   +   8   8   8   8                
     17  32   1           +       8   8   8                    
     18  32   4       +   +   +  10  10  10   2                
     19  32   4           +      11  11  10                    
     1A  32   4           +      10  12  10                    
     1B  32   2       +       +                  16      16    
     1C  64   2       +   +   +  16  16  16  16                
     1D   4   1                                                  Paletted?
     1E   8   1                                                  Paletted?
     20   4   1                                                  ETC1_RGB8 (Ericcon Texture Compression)
     22  16   2                                          16      Float
     23  16   2   +   +                      16                  Float
     24  16   2                                              16  Float
     25  32   2       +       +              16          16      Float
     26  64   2       +   +   +  16  16  16  16                  Float
     2C  32   4                                  24   8          Depth/stencil
     2D  64   4                                                
     2E  48   2           +      16  16  16                    
     2F  48   2           +      16  16  16                      Float
     32  32   4                                                  ?
     3F   0   0                                                  INVALID

    bpp: bits per pixel
    byt: bytes per copy element
    ia: is alpha
    ha: has alpha
    rb/ro: ?
    r/g/b/a/d/s/l/i: red/green/blue/alpha/depth/stencil/luminance/intensity
