# Getting malisc (MALI Shader Compiler)

The MALI Offline Shader Compiler (malisc) can be downloaded from here:
[Offline Shader Compiler](http://www.malideveloper.com/developer-resources/tools/offline-shader-compiler.php)

It comes with some interesting though cryptic documentation on the shader units.

# Compiling a basic fragment shader

Compiling a basic fragment shader is pretty simple. First create a text file called fragment.glsl with the following text:

    void main()
    {
        gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }

Those of you familiar with GLSL should recognise this as the basic identity shader, it simply renders all pixels in red. To compile this shader with malisc we use the following command:

    malisc --frag --verbose --core=Mali-200 -o fragment.mbs fragment.glsl

This compiles a fragment shader for the Mali-200 with verbose output, we'll look at the file it produces later.

# Building the LIMA tools

The LIMA tools are held in gitorious, there are various versions of the tools. The cutting edge versions of the assembly tools can be found in flatmushs-lima, to get them do the following:

    git clone git://gitorious.org/~flatmush/lima/flatmushs-lima.git
    make -C flatmushs-lima/tools/assemble
    make -C flatmushs-lima/tools/mbs_dump

Now the following tools should be built:

 * mbs_dump/disassembler: flatmushs-lima/tools/mbs_dump
 * lima_assembler: flatmushs-lima/tools/assembler

# Disassembling a fragment shader

To disassemble a fragment shader we run mbs_dump on the compiled shader binary.
So to disassemble the fragment shader we compiled earlier execute the following command:

    mbs_dump fragment.mbs

Note: You'll need to ensure that the paths are correctly set or modify the command accordingly.

If you read through the output you'll see various brackets and squiggles, these are specific to the MBS file format as specified here: [[MBS+File+Format]]
If you scroll down to the chunk entitled DBIN, you'll see it contains some interesting looking assembler, this should serve as a reference for the assembly language, along with the readme.txt located in tools/assembler.

# Assembling a basic fragment shader

Now we'll assemble a basic fragment shader from a source file, start by creating a file fragment.s containing the following:

    ^const0 = vec4(1.0, 0.0, 0.0, 1.0),
    $0 = ^const0, stop;

This is a little simpler than what the compiler will create but it does the same thing, now to generate an mbs file (like we did when we compiled the GLSL) do the following:

    lima_assemble fragment.s fragment.mbs

You have now compiled a working fragment shader from assembly.

Note: The assembler is in the early stages so it's not forgiving and doesn't provide many helpful error messages, if you're struggling to assemble a shader you think should be working then:

 * Compile the equivalent shader using malisc and see how the output differs.
 * Ask somebody in #lima on freenode.

# Running a fragment shader on the MALI

For this stage you'll need a working limare install and android device, read [flatmushs-lima/README.android](https://gitorious.org/~flatmush/lima/flatmushs-lima/blobs/master/README.android) from your git clone for more information.

The process of running an assemble fragment shader on the Mali is the same as executing a compiled binary shader so the instructions can be found with the Offline Shader Compiler, however these are a little brief.

It basically boils down to loading the shader using [glShaderBinary](http://www.khronos.org/opengles/sdk/docs/man/xhtml/glShaderBinary.xml).

I have created a small series of samples which can ease the process, to use this approach do the following:

    git clone git://gitorious.org/gl-samples/gl-samples.git
    cd gl-samples/03_binary_shader

To swap in a different shader simply modify or overwrite fragment.s and it will be compiled/uploaded when the following commands are run:

    make -f Makefile.android install
    make -f Makefile.android run

Further examples include 05_texture_shader which expands on the basic shader to add varyings and texturing.
